#include <cstdlib>
#include <array>
#include <iostream>
#include <fstream>
#include <memory>
#include <utility>
#include <vector>

#include <boost/asio.hpp>
#include <boost/algorithm/string.hpp>

using namespace std;
using namespace boost::asio;

// for all
io_service global_io_service;

// panel.cgi
int panel_main(shared_ptr<ip::tcp::socket> html_socket) {
    string str = "Content-type: text/html\r\n\r\n";
    str += R"(
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>NP Project 3 Panel</title>
    <link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
      integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
      crossorigin="anonymous"
    />
    <link
      href="https://fonts.googleapis.com/css?family=Source+Code+Pro"
      rel="stylesheet"
    />
    <link
      rel="icon"
      type="image/png"
      href="https://cdn4.iconfinder.com/data/icons/iconsimple-setting-time/512/dashboard-512.png"
    />
    <style>
      * {
        font-family: 'Source Code Pro', monospace;
      }
    </style>
  </head>
  <body class="bg-secondary pt-5">
    <form action="console.cgi" method="GET">
      <table class="table mx-auto bg-light" style="width: inherit">
        <thead class="thead-dark">
          <tr>
            <th scope="col">#</th>
            <th scope="col">Host</th>
            <th scope="col">Port</th>
            <th scope="col">Input File</th>
          </tr>
        </thead>
        <tbody>
)";
    for (int i = 0; i < 5; i++) {
        str += R"(
          <tr>
            <th scope="row" class="align-middle">Session )";
        str += to_string(i + 1);
        str += R"(</th>
            <td>
              <div class="input-group">
                <select name="h)";
        str += to_string(i);
        str += R"(" class="custom-select">
                  <option></option>)";
        for (int j = 0; j < 12; j++) {
            str += "<option value=\"nplinux";
            str += to_string(j + 1);
            str += ".cs.nctu.edu.tw\">nplinux";
            str += to_string(j + 1);
            str += "</option>";
        }
        str += R"(
                </select>
                <div class="input-group-append">
                  <span class="input-group-text">.cs.nctu.edu.tw</span>
                </div>
              </div>
            </td>
            <td>
              <input name="p)";
        str += to_string(i);
        str += R"(type="text" class="form-control" size="5" />
            </td>
            <td>
              <select name="f{i}" class="custom-select">
                <option></option>)";
        for (int j = 0; j < 10; j++) {
            str += "<option value=\"t";
            str += to_string(j + 1);
            str += ".txt\">t";
            str += to_string(j + 1);
            str += ".txt</option>";
        }
        str += R"(
              </select>
            </td>
          </tr>
        )";
    }
    str += R"(
          <tr>
            <td colspan="3"></td>
            <td>
              <button type="submit" class="btn btn-info btn-block">Run</button>
            </td>
          </tr>
        </tbody>
      </table>
    </form>
  </body>
</html>
    )";
    html_socket->async_send(
        buffer(str, str.size()),
        [](boost::system::error_code ec, size_t length) {
            if (ec) {
                cerr << "panel_main() error" << endl;
            }
        });
    return 0;
}
// --------------------------------------------------------------------------------
// console.cgi
#define SHELL true
vector<string> splited;
vector<array<string, 3>> parameters;

void split_query_str(string query_str) {
    boost::algorithm::split(splited, query_str, boost::algorithm::is_any_of("=&"));
    if (splited.size() != 30) {
        cerr << "QUERY_STRING size incorrect" << endl;
        exit(EXIT_FAILURE);
    }
    for (size_t i = 0; i < splited.size(); i += 6) {
        string host = splited[i + 1];
        string port = splited[i + 3];
        string test = splited[i + 5];
        if (host != "" && port != "" && test != "") {
            parameters.push_back(array<string, 3>({host, port, test}));
        }
    }
}

void print_thead(string &str) {
    for (unsigned int i = 0; i < parameters.size(); i++) {
        str += "          <th scope=\"col\">" + parameters[i][0] + ":" + parameters[i][1] + "</th>\n";
    }
}

void print_tbody(string &str) {
    for (unsigned int i = 0; i < parameters.size(); i++) {
        str += "          <td><pre id=\"s" + to_string(i) + "\" class=\"mb-0\"></pre></td>\n";
    }
}

void print_html(shared_ptr<ip::tcp::socket> html_sock) {
    string str = R"html(Content-type: text/html

    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8" />
        <title>NP Project 3 Console</title>
        <link
        rel="stylesheet"
        href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
        crossorigin="anonymous"
        />
        <link
        href="https://fonts.googleapis.com/css?family=Source+Code+Pro"
        rel="stylesheet"
        />
        <link
        rel="icon"
        type="image/png"
        href="https://cdn0.iconfinder.com/data/icons/small-n-flat/24/678068-terminal-512.png"
        />
        <style>
        * {
            font-family: 'Source Code Pro', monospace;
            font-size: 1rem !important;
        }
        body {
            background-color: #212529;
        }
        pre {
            color: #cccccc;
        }
        b {
            color: #ffffff;
        }
        </style>
    </head>
    <body>
        <table class="table table-dark table-bordered">
        <thead>
            <tr>
    )html";
    print_thead(str);
    str += R"html(
            </tr>
        </thead>
        <tbody>
            <tr>
    )html";
    print_tbody(str);
    str += R"html(
            </tr>
        </tbody>
        </table>
    </body>
    </html>
    )html";
    html_sock->async_send(
        buffer(str, str.size()),
        [](boost::system::error_code ec, size_t length) {
            if (ec) {
                cerr << "do_write_str() error" << endl;
            }
        });
}

void output_to_html(unsigned int id, string str, bool is_from_shell, shared_ptr<ip::tcp::socket> html_sock) {
    boost::algorithm::replace_all(str, "\'", "\\\'");
    boost::algorithm::replace_all(str, "\n", "&NewLine;");
    boost::algorithm::replace_all(str, "\r", "");
    boost::algorithm::replace_all(str, "<", "&#60;");
    boost::algorithm::replace_all(str, ">", "&#62;");
    if (!is_from_shell) {
        str = "<b>" + str + "</b>";
    }
    str = "<script>document.getElementById('s" + to_string(id) + "').innerHTML += '" + str + "';</script>";
    html_sock->async_send(
        buffer(str, str.size()),
        [](boost::system::error_code ec, size_t length) {
            if (ec) {
                cerr << "do_write_str() error" << endl;
            }
        });
}

class NpSession : public enable_shared_from_this<NpSession> {
private:
    enum { max_length = 1024 };
    ip::tcp::socket npshell_socket;
    shared_ptr<ip::tcp::socket> html_socket;
    array<char, max_length> _data;
    unsigned int session_id;
    ifstream fin;

public:
    NpSession(unsigned int id, string file_path, shared_ptr<ip::tcp::socket> html_sock)
    : session_id(id),
      npshell_socket(global_io_service),
      html_socket(html_sock),
      fin("./test_case/" + file_path) {}

    void start(ip::tcp::resolver::iterator iter) {
        if (!fin) {
            session_error("ifstream error");
            return;
        }
        auto self = shared_from_this();
        npshell_socket.async_connect(
            iter->endpoint(),
            [this, self](boost::system::error_code ec) {
                if (ec) {
                    session_error("async_connect() error");
                    return;
                }
                do_read_npshell();
            });
    }

private:
    void session_error(string msg) {
        cerr << "Session " << session_id << " " << msg << endl;
    }

    void do_read_npshell() {
        auto self(shared_from_this());
        npshell_socket.async_read_some(
            buffer(_data, max_length),
            [this, self](boost::system::error_code ec, size_t length) {
                if (!ec) {
                    string str(_data.begin(), _data.begin() + length);
                    output_to_html(session_id, str, SHELL, html_socket);
                    if (str.find("% ") != string::npos) {
                        do_write_npshell();
                    }
                    do_read_npshell();
                }
            });
    }

    void do_write_npshell() {
        auto self(shared_from_this());
        string cmd_str;
        if (getline(fin, cmd_str)) {
            cmd_str += "\n";
            npshell_socket.async_send(
                buffer(cmd_str, cmd_str.size()),
                [this, self, cmd_str](boost::system::error_code ec, size_t length) {
                    if (!ec) {
                        output_to_html(session_id, cmd_str, !SHELL, html_socket);
                    }
                });
        }
    }
};

int console_main(shared_ptr<ip::tcp::socket> html_socket, string query_str) {
    split_query_str(query_str);
    print_html(html_socket);
    ip::tcp::resolver resolver(global_io_service);
    ip::tcp::resolver::iterator res_iter;
    for (unsigned int i = 0; i < parameters.size(); i++) {
        ip::tcp::resolver::query res_query(parameters[i][0], parameters[i][1]);
        res_iter = resolver.resolve(res_query);
        shared_ptr<NpSession> np = make_shared<NpSession>(i, parameters[i][2], html_socket);
        np->start(res_iter);
    }
    parameters.clear();
    return 0;
}
// --------------------------------------------------------------------------------
// http_server
class EchoSession : public enable_shared_from_this<EchoSession> {
private:
    enum { max_length = 1024 };
    shared_ptr<ip::tcp::socket> _socket;
    array<char, max_length> _data;

public:
    EchoSession(shared_ptr<ip::tcp::socket> socket) : _socket(move(socket)) {}

    void start() {
        do_exec_cgi();
    }

private:
    void do_write(string str) {
        auto self(shared_from_this());
        _socket->async_send(
            buffer(str, str.size()),
            [self](boost::system::error_code ec, size_t length) {
                if (ec) {
                    cerr << "do_write_str() error" << endl;
                }
            });
    }

    void do_exec_cgi() {
        auto self(shared_from_this());
        _socket->async_read_some(
            buffer(_data, max_length),
            [this, self](boost::system::error_code ec, size_t length) {
                if (!ec) {
                    vector<string> header_lines, first_line, path_query;
                    boost::algorithm::iter_split(header_lines, _data, boost::algorithm::first_finder("\r\n"));
                    boost::algorithm::split(first_line, header_lines[0], boost::algorithm::is_space());
                    boost::algorithm::split(path_query, first_line[1], boost::algorithm::is_any_of("?"));
                    if (path_query[0] == "/panel.cgi") {
                        do_write("HTTP/1.1 200 OK\r\n");
                        panel_main(_socket);
                    } else if (path_query[0] == "/console.cgi") {
                        do_write("HTTP/1.1 200 OK\r\n");
                        console_main(_socket, path_query[1]);
                    } else {
                        do_write("HTTP/1.1 404\r\n");
                    }
                }
            });
    }
};

class EchoServer {
private:
    ip::tcp::acceptor _acceptor;

public:
    EchoServer(short port) : _acceptor(global_io_service, ip::tcp::endpoint(ip::tcp::v4(), port)) {
        do_accept();
    }

    ~EchoServer() {
        _acceptor.close();
    }

private:
    void do_accept() {
        auto _socket = make_shared<ip::tcp::socket>(global_io_service);
        _acceptor.async_accept(*_socket, [this, _socket](boost::system::error_code ec) {
            if (!ec) {
                make_shared<EchoSession>(move(_socket))->start();
            }
            do_accept();
        });
    }
};

int main(int argc, char *const argv[]) {
    if (argc != 2) {
        cerr << "Usage:" << argv[0] << " [port]" << endl;
        return 1;
    }

    try {
        unsigned short port = atoi(argv[1]);
        EchoServer server(port);
        global_io_service.run();
    } catch (exception &e) {
        cerr << "Exception: " << e.what() << "\n";
    }

    return 0;
}