#include <cstdlib>
#include <array>
#include <iostream>
#include <memory>
#include <utility>
#include <vector>

#include <unistd.h>

#include <boost/asio.hpp>
#include <boost/algorithm/string.hpp>

using namespace std;
using namespace boost::asio;

io_service global_io_service;

class EchoSession : public enable_shared_from_this<EchoSession> {
private:
    enum { max_length = 1024 };
    ip::tcp::socket _socket;
    array<char, max_length> _data;

public:
    EchoSession(ip::tcp::socket socket) : _socket(move(socket)) {}

    void start() {
        do_exec_cgi();
    }

private:
    void do_write(string str) {
        auto self(shared_from_this());
        _socket.async_send(
            buffer(str, str.size()),
            [self](boost::system::error_code ec, size_t length) {
                if (ec) {
                    cerr << "do_write_str() error" << endl;
                }
            });
    }

    void do_exec_cgi() {
        auto self(shared_from_this());
        _socket.async_read_some(
            buffer(_data, max_length),
            [this, self](boost::system::error_code ec, size_t length) {
                if (!ec) {
                    vector<string> header_lines, first_line, path_query;
                    boost::algorithm::iter_split(header_lines, _data, boost::algorithm::first_finder("\r\n"));
                    boost::algorithm::split(first_line, header_lines[0], boost::algorithm::is_space());
                    boost::algorithm::split(path_query, first_line[1], boost::algorithm::is_any_of("?"));
                    if (path_query[0].find(".cgi") != string::npos) {
                        string cgi_name = "." + path_query[0];
                        switch (fork()) {
                            case -1:
                                cerr << "fork() error" << endl;
                                break;
                            case 0:
                                do_write("HTTP/1.1 200 OK\r\n");
                                // setenv
                                clearenv();
                                setenv("REQUEST_METHOD", first_line[0].c_str(), 1);
                                setenv("REQUEST_URI", path_query[0].c_str(), 1);
                                setenv("QUERY_STRING", path_query[1].c_str(), 1);
                                setenv("SERVER_PROTOCOL", "HTTP/1.1", 1);
                                for (auto s : header_lines) {
                                    if (s.find("Host:") != string::npos) {
                                        vector<string> host_line;
                                        boost::algorithm::split(host_line, s, boost::algorithm::is_any_of(" :"));
                                        setenv("HTTP_HOST", host_line[2].c_str(), 1);
                                    }
                                }
                                setenv("SERVER_ADDR", _socket.local_endpoint().address().to_string().c_str(), 1);
                                setenv("SERVER_PORT", to_string(_socket.local_endpoint().port()).c_str(), 1);
                                setenv("REMOTE_ADDR", _socket.remote_endpoint().address().to_string().c_str(), 1);
                                setenv("REMOTE_PORT", to_string(_socket.remote_endpoint().port()).c_str(), 1);
                                // dup
                                dup2(_socket.native_handle(), STDIN_FILENO);
                                dup2(_socket.native_handle(), STDOUT_FILENO);
                                _socket.close();
                                // exec
                                execl(cgi_name.c_str(), cgi_name.c_str(), NULL);
                            default:
                                break;
                        }
                    } else if (path_query[0].find(".cpp") != string::npos) {
                        cerr << "file : " << path_query[0] << endl;
                        if (access(("." + path_query[0]).c_str(), F_OK) == 0) {
                            string cmd = "clang++ ." + path_query[0];
                            cerr << cmd << endl;
                            system(cmd.c_str());
                            cerr << "compile complete" << endl; 
                            if (access("./a.out", F_OK) == 0) {
                                cerr << "file : a.out" << endl;
                                switch(fork()) {
                                    case -1:
                                        do_write("HTTP/1.1 404\r\n");
                                        break;
                                    case 0:
                                        do_write("HTTP/1.1 200 OK\r\n");
                                        // setenv
                                        clearenv();
                                        setenv("REQUEST_METHOD", first_line[0].c_str(), 1);
                                        setenv("REQUEST_URI", path_query[0].c_str(), 1);
                                        setenv("QUERY_STRING", path_query[1].c_str(), 1);
                                        setenv("SERVER_PROTOCOL", "HTTP/1.1", 1);
                                        for (auto s : header_lines) {
                                            if (s.find("Host:") != string::npos) {
                                                vector<string> host_line;
                                                boost::algorithm::split(host_line, s, boost::algorithm::is_any_of(" :"));
                                                setenv("HTTP_HOST", host_line[2].c_str(), 1);
                                            }
                                        }
                                        setenv("SERVER_ADDR", _socket.local_endpoint().address().to_string().c_str(), 1);
                                        setenv("SERVER_PORT", to_string(_socket.local_endpoint().port()).c_str(), 1);
                                        setenv("REMOTE_ADDR", _socket.remote_endpoint().address().to_string().c_str(), 1);
                                        setenv("REMOTE_PORT", to_string(_socket.remote_endpoint().port()).c_str(), 1);
                                        // dup
                                        dup2(_socket.native_handle(), STDIN_FILENO);
                                        dup2(_socket.native_handle(), STDOUT_FILENO);
                                        _socket.close();
                                        // exec
                                        execl("./a.out", "./a.out", NULL);
                                    default:
                                        do_write("HTTP/1.1 404\r\n");
                                        break;
                                }
                            } else {
                                do_write("HTTP/1.1 404\r\n");
                            }
                        } else {
                            do_write("HTTP/1.1 404\r\n");
                        }
                    }
                }
            });
    }
};

class EchoServer {
private:
    ip::tcp::acceptor _acceptor;
    ip::tcp::socket _socket;

public:
    EchoServer(short port)
    : _acceptor(global_io_service, ip::tcp::endpoint(ip::tcp::v4(), port)),
      _socket(global_io_service) {
        do_accept();
    }

private:
    void do_accept() {
        _acceptor.async_accept(_socket, [this](boost::system::error_code ec) {
            if (!ec) {
                make_shared<EchoSession>(move(_socket))->start();
            }
            do_accept();
        });
    }
};

int main(int argc, char *const argv[]) {
    if (argc != 2) {
        cerr << "Usage:" << argv[0] << " [port]" << endl;
        return 1;
    }

    try {
        unsigned short port = atoi(argv[1]);
        EchoServer server(port);
        global_io_service.run();
    } catch (exception &e) {
        cerr << "Exception: " << e.what() << "\n";
    }

    return 0;
}
