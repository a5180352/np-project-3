#include <iostream>
#include <fstream>
#include <vector>
#include <memory>

#include <unistd.h>

#include <boost/asio.hpp>
#include <boost/algorithm/string.hpp>

#define SHELL true

using namespace std;
using namespace boost::asio;

vector<string> splited;
vector<array<string, 3>> parameters;
io_service console_io_service;

void split_query_str() {
    string query_str(getenv("QUERY_STRING"));
    boost::algorithm::split(splited, query_str, boost::algorithm::is_any_of("=&"));
    if (splited.size() != 30) {
        cerr << "QUERY_STRING size incorrect" << endl;
        exit(EXIT_FAILURE);
    }
    for (size_t i = 0; i < splited.size(); i += 6) {
        string host = splited[i + 1];
        string port = splited[i + 3];
        string test = splited[i + 5];
        if (host != "" && port != "" && test != "") {
            parameters.push_back(array<string, 3>({host, port, test}));
        }
    }
}

void print_thead() {
    for (unsigned int i = 0; i < parameters.size(); i++) {
        cout << "          <th scope=\"col\">" << parameters[i][0] << ":" << parameters[i][1] << "</th>" << endl;
    }
}

void print_tbody() {
    for (unsigned int i = 0; i < parameters.size(); i++) {
        cout << "          <td><pre id=\"s" << i << "\" class=\"mb-0\"></pre></td>" << endl;
    }
}

void print_html() {
    cout << R"html(Content-type: text/html

    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8" />
        <title>NP Project 3 Console</title>
        <link
        rel="stylesheet"
        href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
        crossorigin="anonymous"
        />
        <link
        href="https://fonts.googleapis.com/css?family=Source+Code+Pro"
        rel="stylesheet"
        />
        <link
        rel="icon"
        type="image/png"
        href="https://cdn0.iconfinder.com/data/icons/small-n-flat/24/678068-terminal-512.png"
        />
        <style>
        * {
            font-family: 'Source Code Pro', monospace;
            font-size: 1rem !important;
        }
        body {
            background-color: #212529;
        }
        pre {
            color: #cccccc;
        }
        b {
            color: #ffffff;
        }
        </style>
    </head>
    <body>
        <table class="table table-dark table-bordered">
        <thead>
            <tr>)html"
         << endl;
    print_thead();
    cout << R"html(
            </tr>
        </thead>
        <tbody>
            <tr>
        )html"
         << endl;
    print_tbody();
    cout << R"html(
            </tr>
        </tbody>
        </table>
    </body>
    </html>
        )html"
         << endl;
}

void output_to_html(unsigned int id, string str, bool is_from_shell) {
    boost::algorithm::replace_all(str, "\'", "\\\'");
    boost::algorithm::replace_all(str, "\n", "&NewLine;");
    boost::algorithm::replace_all(str, "\r", "");
    boost::algorithm::replace_all(str, "<", "&#60;");
    boost::algorithm::replace_all(str, ">", "&#62;");
    if (!is_from_shell) {
        str = "<b>" + str + "</b>";
    }
    cout << "<script>document.getElementById('s" << id << "').innerHTML += '" << str << "';</script>" << flush;
}

class NpSession : public enable_shared_from_this<NpSession> {
private:
    enum { max_length = 1024 };
    ip::tcp::socket _socket;
    array<char, max_length> _data;
    unsigned int session_id;
    ifstream fin;

public:
    NpSession(unsigned int id, string file_path)
    : session_id(id),
      _socket(console_io_service),
      fin("./test_case/" + file_path) {}

    void start(ip::tcp::resolver::iterator iter) {
        if (!fin) {
            session_exit("ifstream error");
            return;
        }
        auto self = shared_from_this();
        _socket.async_connect(
            iter->endpoint(),
            [this, self](boost::system::error_code ec) {
                if (ec) {
                    session_exit("async_connect() error" + ec.message());
                    return;
                }
                do_read_npshell();
            });
    }

private:
    void session_exit(string msg) {
        cerr << "Session " << session_id << " " << msg << endl;
    }

    void do_read_npshell() {
        auto self(shared_from_this());
        _socket.async_read_some(
            buffer(_data, max_length),
            [this, self](boost::system::error_code ec, size_t length) {
                if (!ec) {
                    string str(_data.begin(), _data.begin() + length);
                    output_to_html(session_id, str, SHELL);
                    if (str.find("% ") != string::npos) {
                        do_write_npshell();
                    }
                    do_read_npshell();
                }
            });
    }

    void do_write_npshell() {
        auto self(shared_from_this());
        string cmd_str;
        if (getline(fin, cmd_str)) {
            cmd_str += "\n";
            _socket.async_send(
                buffer(cmd_str, cmd_str.size()),
                [this, self, cmd_str](boost::system::error_code ec, size_t length) {
                    if (!ec) {
                        output_to_html(session_id, cmd_str, !SHELL);
                    }
                });
        }
    }
};

int main(int argc, char const *argv[]) {
    split_query_str();
    print_html();
    ip::tcp::resolver resolver(console_io_service);
    ip::tcp::resolver::iterator res_iter;
    for (unsigned int i = 0; i < parameters.size(); i++) {
        ip::tcp::resolver::query res_query(parameters[i][0], parameters[i][1]);
        res_iter = resolver.resolve(res_query);
        shared_ptr<NpSession> np = make_shared<NpSession>(i, parameters[i][2]);
        np->start(res_iter);
    }
    console_io_service.run();
    return 0;
}
